import axiosServer from 'axios'

export const axios = axiosServer.create({
  baseURL: 'http://localhost:4567',
  headers: {
  	'Accept': 'application/json',
  	'Content-Type': 'application/json'
  }
});