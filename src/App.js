import React, { Component } from 'react';
import {TabMenu} from 'primereact/tabmenu';
import { Route } from 'react-router-dom';
import ConsultaPessoa from './componentes/ConsultaPessoa'
import ConsultaLancamento from './componentes/ConsultaLancamento'
import CadastroPessoa from './componentes/CadastroPessoa'
import CadastroLancamento from './componentes/CadastroLancamento'
import {MessagesDemo} from './componentes/MessagesDemo'
import './App.css';

class App extends Component {
  state = {
    items: [
      {label: 'Início', icon: 'pi pi-fw pi-home'},
      {label: 'Consulta de Pessoas', icon: 'pi pi-fw pi-search'},
      {label: 'Cadastro de Pessoas', icon: 'pi pi-fw pi-pencil'},
      {label: 'Consulta de Lançamentos', icon: 'pi pi-search'},
      {label: 'Cadastro de Lançamentos', icon: 'pi pi-fw pi-plus'}
    ]
  }

  render() {
    return (
      <div className="App">
        <TabMenu model={this.state.items} />
        <Route path='/ConsultaPessoa' component={ConsultaPessoa}/>
        <Route path='/ConsultaLancamento' component={ConsultaLancamento}/>
        <Route path='/CadastroPessoa' component={CadastroPessoa}/>
        <Route path='/CadastroLancamento' component={CadastroLancamento}/>
        <Route path='/MessagesDemo' component={MessagesDemo}/>
      </div>
    );
  }
}

export default App;
