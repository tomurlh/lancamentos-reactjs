import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class ConsultaLancamento extends Component {
  state = {
    lancamentos: [
    ]

  }



  render() {
    return (
      <div>
        <h1>Lançamentos</h1>
        <hr/>

        <div className="p-datatable p-component">
          <div className="p-datatable-tablewrapper">
            <table>
              <thead className="p-datatable-thead">
                <tr>
                  <th>Tipo</th>
                  <th>Pessoa</th>
                  <th>Descrição</th>
                  <th>Valor</th>
                  <th>Data de Vencimento</th>
                  <th>Pago</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody className="p-datatable-tbody">
              {this.state.lancamentos.map((lancamento, key) => {
                return (     
                  <tr key={key}>
                    <td>
                      {lancamento.tipo === 'RECEITA' &&
                        <img src={require('../imagens/receita.png')} alt="" />
                      }
                      {lancamento.tipo === 'DESPESA' &&
                        <img src={require('../imagens/despesa.png')} alt="" />
                      }
                    </td>
                    <td>{lancamento.pessoa.nome}</td>
                    <td>{lancamento.descricao}</td>
                    <td>{(lancamento.valor).toLocaleString('pt-BR', {
                      style: 'currency',
                      currency: 'BRL'
                    })}</td>
                    <td>{lancamento.dataVencimento}</td>
                    <td>
                      {lancamento.pago &&
                        <img src={require('../imagens/pago.png')} alt="" />
                      }
                      {!lancamento.pago &&
                        <img src={require('../imagens/nao-pago.png')} alt="" />
                      }
                    </td>
                    <td>
                      <Link to={{
                          pathname: '/courses',
                          search: '?sort=name',
                          hash: '#the-hash',
                          state: { fromDashboard: true }}}>
                          <img src={require('../imagens/editar.png')} alt="" style={{ marginRight: 10}} />
                      </Link>
                      <Link to={{
                          pathname: '/courses',
                          search: '?sort=name',
                          hash: '#the-hash',
                          state: { fromDashboard: true }}}>
                          <img src={require('../imagens/excluir.png')} alt="" />
                      </Link>
                    </td>
                  </tr>       
                )
              })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    axios.get(`http://localhost:4567/lancamento`)
      .then(response => {
        console.log(response.data)
        this.setState({
          lancamentos: response.data
        })
      })
  }
}

export default ConsultaLancamento;
