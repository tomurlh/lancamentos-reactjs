import React, { Component } from 'react';
import {InputText} from 'primereact/inputtext';
import {InputMask} from 'primereact/inputmask';
import {Button} from 'primereact/button';
import {Messages} from 'primereact/messages';
import {Message} from 'primereact/message';
import {Dropdown} from 'primereact/dropdown';
import {RadioButton} from 'primereact/radiobutton';
import axios from 'axios';
import '../tabela.css';
import '../primeflex.css';

class CadastroLancamento extends Component {
  state = {
      pessoas: [],
      pessoaSelecionada: { id: '', nome: '' },
      tipo: '',
      pessoa: {nome: ''},
      descricao: '',
      valor: '',
      dataVencimento: '',
      pago: ''
  }

  atualizaNome(event) {
    this.setState({
      nome: event.target.value
    });
  }

  render() {
    return (
      <div>
        <h1>Novo Lancamento</h1>
        <hr/>
        {this.state.descricao}
        <div className="p-grid">
          <div className="p-col"></div>
          <div className="p-col">
            <Messages ref={(el) => this.messages = el} />
            <table className="tabela-cadastro" width="100%">
              <tbody>
                <tr>
                  <td>Tipo</td>
                  <td style={{textAlign: 'left'}}>
                    <RadioButton value="RECEITA" name="tipo"
                      onChange={(e) => this.setState({tipo: e.value})}
                      checked={this.state.tipo === 'RECEITA'} />
                      <span style={{marginRight: 10}}>Receita</span>
                    <RadioButton value="DESPESA" name="tipo"
                      onChange={(e) => this.setState({tipo: e.value})}
                      checked={this.state.tipo === 'DESPESA'} />
                      Despesa
                  </td>
                </tr>
                <tr>
                  <td>Pessoa</td>
                  <td>
                    <Dropdown value={this.state.pessoaSelecionada} options={this.state.pessoas}
                      onChange={(e) => {this.setState({pessoaSelecionada: e.value})}} optionLabel="nome"
                      placeholder="Selecione uma pessoa" style={{width: '100%'}} id="id" />
                  </td>
                </tr>
                <tr>
                  <td>Descrição</td>
                  <td>
                    <InputText
                      value={this.state.descricao}
                      size="60" onChange={(e) => this.setState({descricao: e.value})} />
                  </td>
                </tr>
                <tr>
                  <td>Valor</td>
                  <td>
                    <InputText
                      value={this.state.valor}
                      size="60" onChange={(e) => this.setState({valor: e.value})} />
                  </td>
                </tr>
                <tr>
                  <td>Data de Vencimento</td>
                  <td>
                    <InputMask mask="99/99/9999" value={this.state.dataVencimento}
                      onChange={(e) => this.setState({dataVencimento: e.value})} size="60">
                    </InputMask>
                  </td>
                </tr>
                <tr>
                  <td>Pago</td>
                  <td style={{textAlign: 'left'}}>
                    <RadioButton value="Sim" name="pago"
                      onChange={(e) => this.setState({pago: e.value})}
                      checked={this.state.pago === 'Sim'} />
                      <span style={{marginRight: 10}}>Sim</span>
                    <RadioButton value="Não" name="pago"
                      onChange={(e) => this.setState({pago: e.value})}
                      checked={this.state.pago === 'Não'} />
                      Não
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td><Button label="Cadastrar" onClick={this.cadastrar} /></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="p-col"></div>
        </div>
      </div>
    );
  }

  formularioValido = () => {
    if(!this.state.tipo || this.state.pessoaSelecionada || this.state.descricao ||
       this.state.valor || this.state.dataVencimento || this.state.pago)
      return false

    return true
  }


  cadastrar = () => {
    if(this.formularioValido()) {
      axios.post('http://localhost:4567/lancamento/salvar', {
        tipo: this.state.tipo,
        pessoaId: this.state.pessoaSelecionada.id,
        descricao: this.state.descricao,
        valor: this.state.valor,
        dataVencimento: this.state.dateVencimento,
        pago: this.state.pago
      })
      .then(response => {
        this.messages.show({severity: 'success', detail: 'Cadastro efetuado com sucesso'});
        this.setState({ nome: '' })
      })
    }
    else {
      this.messages.show({severity: 'error', detail: 'Preencha o formulário corretamente'});
      this.setState({mensagem: 'Preencha o formulário corretamente'})
    }
  }

  componentDidMount() {
    axios.get('http://localhost:4567/pessoa')
    .then(response => {
      console.log(response.data)
      this.setState({ pessoas: response.data })
    })
    .catch(error => { console.log(error) })
  }
}

export default CadastroLancamento;