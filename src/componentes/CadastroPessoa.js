import React, { Component } from 'react';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Messages} from 'primereact/messages';
import {Message} from 'primereact/message';
import axios from 'axios';
import '../tabela.css';
import '../primeflex.css';

class CadastroPessoa extends Component {
  state = {
      nome: ''
  }

  atualizaNome(event) {
    this.setState({
      nome: event.target.value
    });
  }

  clear = () => {
    console.log('teste');
  }

  render() {

    return (
      <div>
        <h1>Nova Pessoa</h1>
        <hr/>
        <div className="p-grid">
          <div className="p-col"></div>
          <div className="p-col">
          <Messages ref={(el) => this.messages = el} />
            <table className="tabela-cadastro" width="100%">
              <tbody>
                <tr>
                  <td>Nome</td>
                  <td>
                    <InputText
                      value={this.nome} size="60"
                      onChange={this.atualizaNome.bind(this)}
                    />
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td><Button label="Cadastrar" onClick={this.cadastrar} /></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="p-col"></div>
        </div>
      </div>
    );
  }

  formularioValido = () => {
    if(this.state.nome == '')
      return false
    return true
  }


  cadastrar = () => {
    if(this.formularioValido()) {
      axios.post('http://localhost:4567/pessoa/salvar', { nome: this.state.nome })
      .then(response => {
        this.messages.show({severity: 'success', detail: 'Cadastro efetuado com sucesso'});
        this.setState({ nome: '' })
      })
    }
    else {
      this.messages.show({severity: 'error', detail: 'Preencha o formulário corretamente'});
      this.setState({mensagem: 'Preencha o formulário corretamente'})
    }
  }
}

export default CadastroPessoa;