import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class ConsultaPessoa extends Component {
  state = {
    pessoas: [
    ]

  }

  render() {
    return (
      <div>
        <h1>Pessoas</h1>
        <hr/>

        <div className="p-datatable p-component">
          <div className="p-datatable-tablewrapper">
            <table>
              <thead className="p-datatable-thead">
                <tr>
                  <th>Nome</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody className="p-datatable-tbody">
              {this.state.pessoas.map((pessoa, key) => {
                return (
                  <tr key={key}>
                    <td>{pessoa.nome}</td>
                    <td>
                      <Link to={{
                          pathname: '/courses',
                          search: '?sort=name',
                          hash: '#the-hash',
                          state: { fromDashboard: true }}}>
                          <img src={require('../imagens/editar.png')}
                            style={{ marginRight: 10}} alt=""
                          />
                      </Link>
                      <Link to={{
                          pathname: '/courses',
                          search: '?sort=name',
                          hash: '#the-hash',
                          state: { fromDashboard: true }}}>
                          <img src={require('../imagens/excluir.png')}
                            style={{ marginRight: 10}} alt=""
                          />
                      </Link>
                    </td>
                  </tr>
                )
              })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    axios.get(`http://localhost:4567/pessoa`)
      .then(response => {
        this.setState({
          pessoas: response.data
        })
      })
  }
}

export default ConsultaPessoa;
