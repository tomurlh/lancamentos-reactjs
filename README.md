## Lançamentos React.js

Projeto desenvolvido com [React.js](https://reactjs.org/) que consome uma [API](https://gitlab.com/tomurlh/lancamentos-api.git) escrita com [Spark Java Framework](http://sparkjava.com) e [JPA](https://docs.oracle.com/javaee/7/api/javax/persistence/package-summary.html).

O projeto foi desenvolvido para realização de estudo comparativo entre Framework Java (JSF) e JavaScript (React.js), tema de monografia da pós-graduação em *Desenvolvimento de Sistemas com Java*.

A aplicação realiza consulta, cadastro, atualização e remoção de pessoas e lançamentos financeiros.

### `front-end`


| Tecnologia  | Descrição |
| ------------- | ------------- |
| ![](https://gitlab.com/tomurlh/lancamentos-reactjs/raw/master/src/imagens/react-logo-small.png)  | [React](https://reactjs.org/) -> usado para a contrução das views  |
| ![](https://gitlab.com/tomurlh/lancamentos-reactjs/raw/master/src/imagens/primereact-logo-small.png)  | [PrimeReact](https://www.primefaces.org/primereact/) -> biblioteca de componentes para o JSF  |
| ![](https://gitlab.com/tomurlh/lancamentos-reactjs/raw/master/src/imagens/react-router-logo-small.jpg)  | [ReactRouter](https://github.com/ReactTraining/react-router) -> biblioteca para navegação entre views através da rota da aplicação  |
| ![](https://gitlab.com/tomurlh/lancamentos-reactjs/raw/master/src/imagens/axios-imagem-small.png)  | [Axios](https://github.com/axios/axios) -> consome as rotas da API e obtém as respostas  |

### `back-end`

| Tecnologia  | Descrição |
| ------------- | ------------- |
| ![](https://gitlab.com/tomurlh/lancamentos-reactjs/raw/master/src/imagens/spark-logo-small.png)  | [Spark Java Framework](http://sparkjava.com) -> criação das rotas da API, receber as requisições e devolver a resposta  |
| ![](https://gitlab.com/tomurlh/lancamentos-reactjs/raw/master/src/imagens/jpa-imagem-small.png)  | [Java Persistence API](https://docs.oracle.com/javaee/7/api/javax/persistence/package-summary.html) -> usado na API para interagir com o banco de dados  |
| ![](https://gitlab.com/tomurlh/lancamentos-reactjs/raw/master/src/imagens/jackson-imagem-small.png)  | [Jackson](https://github.com/FasterXML/jackson-databind) -> usado para converter os objetos Java capturados do banco para JSON  |